package com.pack.test1;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.InstanceOf;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.pack.controller.EmployeeController;
import com.pack.model.Employee;
import com.pack.service.EmployeeService;

//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = { TestBeanConfig.class })
public class EmployeeControllerTest {

	    @Mock
	    EmployeeService employeeService;

	    @InjectMocks
	    EmployeeController employeeController;

	    private MockMvc mockMvc;

	    @Before
	    public void setuo(){
	        MockitoAnnotations.initMocks(this);
	        mockMvc = MockMvcBuilders.standaloneSetup(employeeController).build();
	    }

	    @Test
	    public void testFetchAllEmployee() throws Exception{

	        List<Employee> employees = new ArrayList<Employee>();
	        employees.add(new Employee());
	        employees.add(new Employee());

	        when(employeeService.fetchEmployee()).thenReturn((List) employees);

	        mockMvc.perform(get("/fetch"))
	                .andExpect(status().isOk())
	                .andExpect(view().name("EmployeeSuccess"))
	                .andExpect(model().attribute("employeelist", hasSize(2)));
	    }

	    @Test
	    public void testFetchEmployeeById() throws Exception {
	        Integer id = 1;

	        when(employeeService.fetchEmployeeById(id)).thenReturn(new Employee());

	        mockMvc.perform(get("/fetchById/1"))
	                .andExpect(status().isOk())
	                .andExpect(view().name("EmployeeUpdate"))
	                .andExpect(model().size(1))
	                .andExpect(model().attributeExists("editForm"))
	                .andExpect(model().attribute("editForm",new InstanceOf(Employee.class)));
	    }
	    
	    
	    @Test
	    public void testNewEmployee() throws Exception {
	        verifyZeroInteractions(employeeService);
	        mockMvc.perform(get("/redirect"))
	                .andExpect(status().isOk())
	                .andExpect(view().name("EmployeeRegister"))
	                //.andExpect(forwardedUrl("EmployeeRegister"))
	                .andExpect(model().attribute("empForm", new InstanceOf(Employee.class)));
	    }


		@Test
	    public void updateEmployeeTest(){
	        Employee employee = new Employee(111,"Danile",24,24000.0,"BE","sam@gmail.com",new Date());
	        Mockito.when(employeeService.fetchEmployeeById(Mockito.anyInt())).thenReturn(employee);
	        ArgumentCaptor<Employee> argCap=ArgumentCaptor.forClass(Employee.class);
	        employee.setQualification("MBBS");
	        employeeService.updateEmployee(employee);
	        Mockito.verify(employeeService).updateEmployee(argCap.capture());
	        assertEquals("MBBS",argCap.getValue().getQualification());
	    }
		
		 @Test
		    public void testDeleteEmployeeById() throws Exception {
			 Integer id = 1;

		        mockMvc.perform(get("/delete/1"))
		                .andExpect(status().is3xxRedirection()) //status code in 300 range
		                .andExpect(view().name("redirect:/fetch"));

		        verify(employeeService, times(1)).deleteEmployee(id);
		    }
        
			@Test
		    public void saveUserTest() throws Exception{
		        Integer id=1;
		        Date doj=new Date();
		        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		        String sdoj=sdf.format(doj);
		        mockMvc.perform(post("/saveEmp")
		                .param("id", "1")
		                .param("name", "Daniles")
		                .param("age", "24")
		                .param("salary", "24000.0")
		                .param("qualification", "BE")
		                .param("email", "sam@gmail.com")
		                .param("doj", sdoj))
		                .andExpect(status().is2xxSuccessful())
		                .andExpect(view().name("EmployeeSuccess"))
		                .andExpect(model().attribute("empForm", new InstanceOf(Employee.class)))
		                .andExpect(model().attribute("empForm", hasProperty("name", is("Daniles"))))
		                .andExpect(model().attribute("empForm", hasProperty("age", is(24))))
		                .andExpect(model().attribute("empForm", hasProperty("salary", is(24000.0))))
		                .andExpect(model().attribute("empForm", hasProperty("qualification", is("BE"))));
		                
		        ArgumentCaptor<Employee> argCap=ArgumentCaptor.forClass(Employee.class);
		        Mockito.verify(employeeService).saveEmployee(argCap.capture());
		        assertEquals("Daniles",argCap.getValue().getName());
		    }
			
			@Test
		    public void saveUserTestValidationError() throws Exception{
		        Integer id=1;
		        Date doj=new Date();
		        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		        String sdoj=sdf.format(doj);
		        mockMvc.perform(post("/saveEmp")
		                .param("id", "2")
		                .param("name", "Ram")
		                .param("age", "24")
		                .param("salary", "24000.0")
		                .param("qualification", "BE")
		                .param("email", "sam@gmail.com")
		                .param("doj", sdoj))
		                .andExpect(status().is2xxSuccessful())
		                .andExpect(view().name("EmployeeRegister"))
		                .andExpect(model().attribute("empForm", new InstanceOf(Employee.class)))
		                .andExpect(model().attribute("empForm", hasProperty("name", is("Ram"))))
		                .andExpect(model().attribute("empForm", hasProperty("age", is(24))))
		                .andExpect(model().attribute("empForm", hasProperty("salary", is(24000.0))))
		                .andExpect(model().attribute("empForm", hasProperty("qualification", is("BE"))));
		                
		        ArgumentCaptor<Employee> argCap=ArgumentCaptor.forClass(Employee.class);
		        Mockito.verifyZeroInteractions(employeeService);
		        
		    }
		}

