package com.pack.test;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.pack")
public class TestBeanConfig {

}
