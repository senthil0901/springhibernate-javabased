//https://www.infoworld.com/article/3543268/junit-5-tutorial-part-2-unit-testing-spring-mvc-with-junit-5.html?page=3
package com.pack.test;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.pack.controller.EmployeeController;
import com.pack.dao.EmployeeDao;
import com.pack.model.Employee;
import com.pack.service.EmployeeService;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = { TestBeanConfig.class })
public class EmployeeTest {
	@Autowired
	EmployeeService service;

	@Mock
	HttpServletRequest request;
	@Mock
	EmployeeDao dao;
	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@InjectMocks
	private EmployeeController employeeController;

	Employee emp;

	@Before
	public void setUp() {
		emp = new Employee();
		MockitoAnnotations.initMocks(this);
	this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void listEmployees() {
		assertEquals(2, service.fetchEmployee().size());
	}

	@Test
	public void registrationTest() throws Exception {
		mockMvc.perform(get("/redirect")).andExpect(status().isOk()).andExpect(view().name("EmployeeRegister"));
	}
	
	@Test
	public void listEmployeeTest() throws Exception {
		mockMvc.perform(get("/fetch")).andExpect(status().isOk()).andExpect(view().name("EmployeeSuccess"));
	}
	
	/*
	@Test
    public void testdeleteEmployee() throws Exception {
        mockMvc.perform(get("/delete/44799"))
               .andExpect(status().isMovedTemporarily())
               .andExpect(MockMvcResultMatchers.redirectedUrl("/fetch"));
    }
	
	@Test
    public void testFetchEmployee() throws Exception {
        mockMvc.perform(get("/fetch"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("EmployeeSuccess"))
                    .andExpect(forwardedUrl("/WEB-INF/views/EmployeeSuccess.jsp"))
                    .andExpect(model().attribute("employeeList", hasSize(2)));
    }
	
	@Test
	 public void testFetchEmployeeById() throws Exception {
	        mockMvc.perform(get("/fetchById/44799"))
	               .andExpect(status().isOk())
	               .andExpect(view().name("EmployeeUpdate"))
	               .andExpect(forwardedUrl("/WEB-INF/views/EmployeeUpdate.jsp"))
	               .andExpect(model().size(1))
	               .andExpect(model().attributeExists("editForm"));
	 }
	
	@Test
    public void updateEmployeeTest(){
        Employee employee = new Employee(111,"Danile",24,24000.0,"BE","sam@gmail.com",new Date());
        Mockito.when(dao.fetchEmployeeById(Mockito.anyInt())).thenReturn(employee);
        ArgumentCaptor<Employee> argCap=ArgumentCaptor.forClass(Employee.class);
        employee.setQualification("MBBS");
        dao.updateEmployee(employee);
        Mockito.verify(dao).updateEmployee(argCap.capture());
        assertEquals("MBBS",argCap.getValue().getQualification());
    }
	*/
	/*@Test
    public void saveEmployee_doNothing_when() {
        Employee employee = new Employee(111,"Danile",24,24000.0,"BE","sam@gmail.com",new Date());
        Mockito.doNothing().when(dao).saveEmployee(Mockito.any(Employee.class));
        service.saveEmployee(employee);
    }*/
	
	/*@Test
    public void saveUserTest() {
        Employee employee = new Employee(111,"Danile",24,24000.0,"BE","sam@gmail.com",new Date());
        ArgumentCaptor<Employee> argCap=ArgumentCaptor.forClass(Employee.class);
        dao.saveEmployee(employee);
        //Mockito.verify(dao,Mockito.times(1)).saveEmployee(employee);
        Mockito.verify(dao).saveEmployee(argCap.capture());
        assertEquals("Danile",argCap.getValue().getName());
    }*/

	/*@Test
	 public void saveEmployeeValidationNameError() throws Exception {
	        Employee employee = new Employee(2020,"Danile",24,24000.0,"BE","sam@gmail.com",new Date());
	        mockMvc.perform(post("/saveEmp").contentType(MediaType.APPLICATION_JSON).content(asJsonString(employee)))
	        .andExpect(status().isOk())
	        .andExpect(view().name("EmployeeSuccess"))
	        .andExpect(forwardedUrl("/WEB-INF/views/EmployeeSuccess.jsp"));
	 }*/
	
	/*@Test
	public void saveUser() throws Exception{
		Employee empl=new Employee(2000,"Senthil",30,30000.0,"BE","sam@gmail.com",new Date());
		mockMvc.perform(post("/saveEmp")
				.param("id", String.valueOf(empl.getId()))
				.param("name", empl.getName())
				.param("age",String.valueOf(empl.getAge()))
				.param("salary", String.valueOf(empl.getSalary()))
				.param("qualification", empl.getQualification())
				.param("email", empl.getEmail())
				.param("doj", String.valueOf(empl.getDoj()))
				.flashAttr("employeeList", service.fetchEmployee()))
		        .andExpect(status().isFound())
	        .andExpect(view().name("EmployeeRegister"));
	        
	}*/

}
