<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Updation</title>
</head>
<body>
	<h3>Employee Updation Form</h3>

	<form:form action="/SpringHibernate-JavaBased/editEmp" method="post" commandName="editForm" novalidate="novalidate">


		<table>
		<tr>
				<td>Employee Id:</td>
				<td><form:input path="id" readonly="true" /></td>
			</tr>
			<tr>
				<td>Employee Name:</td>
				<td><form:input path="name" readonly="true"/></td>
				
			</tr>
			<tr>
				<td>Age:</td>
				<td><form:input path="age" /></td>
				<td><font color="red"><form:errors path="age" /></font></td>
			</tr>
			<tr>
				<td>Salary:</td>
				<td><form:input path="salary" /></td>
				<td><font color="red"><form:errors path="salary" /></font></td>
			</tr>
			<tr>
				<td>Qualification:</td>
				<td><form:input path="qualification" /></td>
				<td><font color="red"><form:errors path="qualification" /></font></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input type="email" path="email" /></td>
				<td><font color="red"><form:errors path="email" /></font></td>
			</tr>
			<tr>
				<td>Date of Join:</td>
				<td><form:input type="date" path="doj" /></td>
				<td><font color="red"><form:errors path="doj" /></font></td>
			</tr>
			<tr>
				<td><input type="submit" value="Edit Employee" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>