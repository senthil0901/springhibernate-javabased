package com.pack.controller;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pack.model.Employee;
import com.pack.service.EmployeeService;

@Controller
public class EmployeeController {

	private static Logger log = Logger.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	public String load_registration_page(ModelMap m) {
		log.info("Request inside loadRegistration page");
		Employee e = new Employee();
		m.addAttribute("empForm", e);
		return "EmployeeRegister";
	}

	@RequestMapping(value = "/saveEmp", method = RequestMethod.POST)
	public String saveEmployee(@Validated @ModelAttribute("empForm") Employee e, BindingResult result, ModelMap m) {

		String viewPage;
		if (result.hasErrors()) {
			log.info("validation errors occured");
			viewPage = "EmployeeRegister";
		} else {
			log.info("invoking save employee method");
			Random r = new Random();
			int empid = r.nextInt(99999) + 10000;
			e.setId(empid);
			employeeService.saveEmployee(e);
			List<Employee> emplist = employeeService.fetchEmployee();
			m.addAttribute("employeelist", emplist);
			viewPage = "EmployeeSuccess";
		}
		return viewPage;
	}

	@RequestMapping(value = "/fetchById/{empId}")
	public String fetchEmployeeById(@PathVariable("empId") Integer emplId, ModelMap m) {
		log.info("fetching employee by id");
		Employee e = employeeService.fetchEmployeeById(emplId);
		m.addAttribute("editForm", e);
		return "EmployeeUpdate";

	}

	@RequestMapping(value = "/editEmp", method = RequestMethod.POST)
	public String updateEmployee(@Validated @ModelAttribute("editForm") Employee e, BindingResult result, ModelMap m) {
		String editViewPage;
		if (result.hasErrors()) {
			log.info("validation errors occured");
			editViewPage = "EmployeeUpdate";
		} else {
			log.info("invoking method to update the data");
			employeeService.updateEmployee(e);
			List<Employee> emplist = employeeService.fetchEmployee();
			m.addAttribute("employeelist", emplist);
			editViewPage = "EmployeeSuccess";
		}
		return editViewPage;
	}

	@RequestMapping(value = "/delete/{empId}")
	public String deleteByEmployee(@PathVariable("empId") Integer eid, ModelMap m) {
		log.info("deleting employee based on employee id");
		employeeService.deleteEmployee(eid);
		
		return "redirect:/fetch";
	}
	
	
	@RequestMapping(value="/fetch")
	public String fetchEmployee(ModelMap m)
	{
		List<Employee> empList=employeeService.fetchEmployee();
		log.info("Fetched employee "+empList.size());
		m.addAttribute("employeelist",empList);
		return "EmployeeSuccess";
	}
}
