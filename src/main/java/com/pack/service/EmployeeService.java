package com.pack.service;

import java.util.List;

import com.pack.model.Employee;

public interface EmployeeService {
	public void saveEmployee(Employee e);

	public List<Employee> fetchEmployee();

	public Employee fetchEmployeeById(Integer empid);
	public void updateEmployee(Employee e);
	public void deleteEmployee(Integer eid);
}
