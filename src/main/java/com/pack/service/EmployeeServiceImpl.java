package com.pack.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pack.dao.EmployeeDao;
import com.pack.model.Employee;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	private static Logger log = Logger.getLogger(EmployeeServiceImpl.class);
	@Autowired
	private EmployeeDao employeedao;

	@Override
	public void saveEmployee(Employee e) {
		// TODO Auto-generated method stub
		log.info("save employee method");
		employeedao.saveEmployee(e);
		
	}

	@Override
	public List<Employee> fetchEmployee() {
		// TODO Auto-generated method stub
		log.info("fetch employee method");
		List<Employee> emplist = employeedao.fetchEmployee();
		return emplist;
	}

	@Override
	public Employee fetchEmployeeById(Integer empid) {
		// TODO Auto-generated method stub
		log.info("fetch employeebyid method");
		Employee e = employeedao.fetchEmployeeById(empid);
		return e;
	}

	@Override
	public void updateEmployee(Employee e) {
		// TODO Auto-generated method stub
		log.info("update employee method");
		employeedao.updateEmployee(e);
	}

	@Override
	public void deleteEmployee(Integer eid) {
		// TODO Auto-generated method stub
		log.info("delete employee method");
		employeedao.deleteEmployee(eid);
	}

}
