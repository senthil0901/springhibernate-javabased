package com.pack.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pack.model.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	private static Logger log = Logger.getLogger(EmployeeDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveEmployee(Employee e) {
		// TODO Auto-generated method stub
		log.info(" Dao save employee method");
		sessionFactory.getCurrentSession().save(e);
		
	}

	@Override
	public List<Employee> fetchEmployee() {
		// TODO Auto-generated method stub
		log.info(" Dao fetch employee method");
		Query q = sessionFactory.getCurrentSession().createQuery("from Employee emp");
		List<Employee> emplist = q.list();

		return emplist;
	}

	@Override
	public Employee fetchEmployeeById(Integer empid) {
		// TODO Auto-generated method stub
		log.info(" Dao fetch employeebyid method");
		Employee e = (Employee) sessionFactory.getCurrentSession().get(Employee.class, empid);

		return e;
	}

	@Override
	public void updateEmployee(Employee e) {
		// TODO Auto-generated method stub
		log.info(" Dao update employee method");
		sessionFactory.getCurrentSession().update(e);
	}

	@Override
	public void deleteEmployee(Integer eid) {
		// TODO Auto-generated method stub
		log.info(" Dao delete employee method");
		Query q = sessionFactory.getCurrentSession().createQuery("delete from Employee e where e.id=:empid");
		q.setParameter("empid", eid);
		q.executeUpdate();
	}

}
