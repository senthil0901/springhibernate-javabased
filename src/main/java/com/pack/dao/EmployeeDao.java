package com.pack.dao;

import java.util.List;

import com.pack.model.Employee;

public interface EmployeeDao {
	public void saveEmployee(Employee e);

	public List<Employee> fetchEmployee();

	public Employee fetchEmployeeById(Integer empid);
	public void updateEmployee(Employee e);
	public void deleteEmployee(Integer eid);
}
