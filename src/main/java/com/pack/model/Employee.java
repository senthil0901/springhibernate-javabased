package com.pack.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name="empl1000")
public class Employee {
	@Id	
	private Integer id;
	@NotEmpty
	@Size(min = 6, max = 15)
	private String name;
	@NotNull
	@Min(value = 18)
	@Max(value = 50)
	private Integer age;
	@NotNull
	private Double salary;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotEmpty
	private String qualification;
	@NotEmpty
	@Email
	private String email;
	@NotNull
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date doj;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Employee(Integer id, String name, Integer age, Double salary, String qualification, String email, Date doj) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.qualification = qualification;
		this.email = email;
		this.doj = doj;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
